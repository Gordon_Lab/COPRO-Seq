COPRO-Seq
=========
Scripts and other resources needed to perform a COPRO-Seq (COmmunity PROfiling by Sequencing) analysis as originally described by McNulty and colleagues (1,2). This type of analysis is useful when one is interested in simultaneously measuring the proportional representation of many different microbes in an experimental sample. It is best suited for scenarios in which a researcher is working with defined consortia of known, sequenced organisms.

In recent years Gordon Lab researchers have continued to make updates and refinements to the COPRO-Seq pipeline (3). Consequently, the version stored here supersedes a previous one archived at https://github.com/nmcnulty/COPRO-Seq. 

Those maintaining this resource are pleased to make it available to the research community. However, it should be noted that all versions of the pipeline constructed to date have been principally intended for in-house use on the high-throughput computing facility maintained at the Center for Genome Sciences and Systems Biology at Washington University in St. Louis. Because these resources were not designed with widespread distribution in mind, Gordon Lab staff are unable to provide assistance at this time to those interested in running the COPRO-Seq pipeline in their own computing environments.

References
----------
1) McNulty, NP, Yatsunenko, T, and Hsiao A, et al. Sci Transl Med. 2011 Oct 26;3(106):106ra106. https://doi.org/10.1126/scitranslmed.3002701
2) McNulty, NP, et al. PLoS Biol. 2013;11(8):e1001637.  https://doi.org/10.1371/journal.pbio.1001637
3) Hibberd MC, et al. Sci Transl Med. 2017 May 17;9(390). pii: eaal4069. https://doi.org/10.1126/scitranslmed.aal4069