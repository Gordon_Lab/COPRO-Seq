Files downloaded from NCBI FTP site at:
ftp://ftp.ncbi.nih.gov/genomes/M_musculus/Assembled_chromosomes/seq/

Concatenations for each genome (either MGSC or celera) include all 
FASTA files except those ending in unplaced.fa.

Mus_musculus_Build37-2.fna is a concatenation of the MGSC and celera
concatenations.

======================================================================
Organism: Mus musculus (mouse)
NCBI Build Number: 37
Version: 2
Release date: 04 March 2011

Freeze date for component genomic sequences: 25 January 2007
Freeze date for other genomic sequences: 07 July 2010
Freeze date for mRNAs/ESTs used for annotation: 07 July 2010

This build consists of a reference assembly and an alternate assembly 
both representing the whole genome.

Assemblies in NCBI Build 37.2
Label           Description
-----           -----------
MGSCv37         The mouse genome reference assembly, produced by NCBI 
		in consultation with the Mouse Genome Sequencing 
		Consortium, that contains small amounts of WGS and 
		HTGS Draft sequence. The full MGSCv37 assembly is 
		comprised of 15 assembly-units which have 
		the following labels:
                
		C57BL/6J       Primary assembly, 
			produced by NCBI in consultation with the 
			Mouse Genome Sequencing Consortium, that 
			contains small amounts of WGS and HTGS Draft 
			sequence.
		
		129/J          |  
		129/Ola        | 
		129/Sv         |  
		129/SvEvTac    |
		129/SvJ        |Alternate locus groups
		129S7/SvEv     |Finished sequence from clones of one
		A/J            |strain, or sub-strain, assembled into 
		AKR/J          |contigs and reviewed by NCBI staff.
		BALB/c         |
		Cast/Ei        |
		NOD            |
		Spret/Ei       |
		unknown strain |
		
		non-nuclear
		
                The primary assembly (strain C57BL/6J) represents the 
		assembled chromosomes, plus any unlocalized or 
		unplaced sequence that represent the non-redundant, 
		haploid assembly. The 13 alternate locus groups 
		contain contigs from other strains and represent 
		regions for which there is large scale variation from 
		the sequence in the primary assembly.  
		For more details, see:
	        http://www.ncbi.nlm.nih.gov/genome/assembly/grc/mouse/index.shtml
                
		The non-nuclear assembly-unit contains the NCBI 
		Reference Sequence (RefSeq) for the mouse 
		mitochondrion genome.

                Some clones were annotated in NCBI Build 37.1 but dropped 
		from the alternate locus groups for NCBI Build 37.2. 
		Only clones which add genes not represented in the 
		C57BL/6J assembly-unit or which occur in regions where a 
		publication defines an inter-strain difference were 
		retained in the alternate locus.  
		The list of dropped clones is in the file dropped_scaffolds
		
	        MGSCv37 is the reference assembly.
	
Mm_Celera       Celera Genomics whole genome shotgun assembly 
                (AAHY00000000.1, June 2007). 


======================================================================
Note:
The files in this directory currently contain no STS features. 
These features will be added in the near future.
======================================================================
