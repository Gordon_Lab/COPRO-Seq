#!/usr/bin/env perl

use strict;
use warnings;

use FindBin qw($Bin);

my $instructions_location = "$Bin/sbatch";

`cp $instructions_location\/* .`;
